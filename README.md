# README #
Bit8 Task 3

### Quick summary ###
Simple todo list application, unfortunately without animations


### FEATURES ###

* Ordering by Drag And Drop
* Creating a task by hit entering
* Search
* Delete



### Demo ###

[APP DEMO](https://aklesky-bit8-task1.herokuapp.com/)


### CORE ###
[Angular](https://angular.io)
[Sortable](https://valor-software.com/ngx-bootstrap/#/sortable)

### UI ### 
* [bootstrap3](http://getbootstrap.com/)



### Node Version ###
* node v8.1.1
* npm 5.0.3

### Continuous Integration and Delivery ###
* Circle CI ( eslint test)
* Circle CI Continuous Delivery to Heroku
