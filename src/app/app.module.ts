import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { ModalComponent } from './modal/modal.component';
import { ItemComponent } from './todo/item/item.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    ModalComponent,
    ItemComponent,
  ],
  imports: [
    SortableModule.forRoot(),
    ModalModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})
export class AppModule { }
