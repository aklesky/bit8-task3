import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'todo-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  constructor() { }

  @Input()
  item: any;

  @Output()
  onClick: EventEmitter<any[]> = new EventEmitter<any[]>()

  @Output()
  onChange: EventEmitter<any[]> = new EventEmitter<any[]>()

  ngOnInit() {

  }

  onClickEvent() {
    this.onClick.emit(this.item);
  }

  onChangeEvent() {
    this.onChange.emit(this.item);

  }

}
