
import { BaseModel } from '../../types'

export class Task implements BaseModel {
  constructor(public id: number, public title: string,
    public isMarkedAsDone: boolean, public priority: number) { }

  setTitle(_title: string): Task {
    this.title = _title;
    return this;
  }

  setIsMarkAsDone(_isMarkedAsDone: boolean): Task {
    this.isMarkedAsDone = _isMarkedAsDone;
    return this;
  }

  setPriority(_priority: number): Task {
    this.priority = _priority;
    return this;
  }

  update(item: Task) {
    this.title = item.title;
    this.isMarkedAsDone = item.isMarkedAsDone;
    this.priority = item.priority;
    return this;
  }

}
