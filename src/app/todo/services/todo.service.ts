import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';
import { Task } from '../models/task';
import { StorageProvider } from '../../types'


@Injectable()
export class TodoService implements StorageProvider {

  _key = "tasks";
  collection: Task[] = [];


  constructor() {
  }


  getTasks(): Observable<Task[]> {
    return Observable.from([this.getAll()]);
  }

  onSearchTask(searchTerm: string): Observable<Task[]> {
    const searchResult: Task[] = this.collection.filter((el: Task) => {
      const pattern = new RegExp(searchTerm, 'ig');
      return el.title.match(pattern);
    })
    return Observable.from([searchResult]);
  }

  editTask(item: Task) {
    const index = this.findIndex(item);
    this.collection[index] = item;;
    this.save();
  }

  reorderTasks(collection: Task[]) {
    for (var i = 0, y = collection.length; i < y; i++) {
      const task = collection[i];
      task.priority = i;

    }
    this.update(collection);
  }

  public setItem(item: Task) {
    if (this.collection !== null) {
      this.collection.push(item);
      this.save();
    }
    return this;
  }

  getAll(): Task[] {
    let collection = JSON.parse(localStorage.getItem(this._key)) || [];
    collection = collection.sort((a, b) => {
      if (b.priority < a.priority) return 1;
      if (b.priority > a.priority) return -1;
      return 0;
    });
    this.collection = collection;
    return this.collection;
  }

  save(): StorageProvider {
    this.update(this.collection);
    return this;
  }

  removeItem(item) {
    if (!this.collection.length) return;
    const index = this.findIndex(item);
    this.collection.splice(index, 1);
    this.save();
  }

  findIndex(item: Task): number {
    const index = this.collection.findIndex(element => element.id === item.id);
    return index;
  }

  update(newValue) {
    localStorage.setItem(this._key, JSON.stringify(newValue || []));
    return this;
  }
}
