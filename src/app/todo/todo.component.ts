import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/startWith';
import { SortableComponent } from 'ngx-bootstrap/sortable';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
const uuidv4 = require('uuid/v4');

import { ModalComponent } from '../modal/modal.component';
import { TodoService } from './services/todo.service';
import { Task } from './models/task';


@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  providers: [TodoService],
})
export class TodoComponent implements OnInit {
  private searchTerms = new Subject<string>();
  private observable: Observable<Task[]>;
  private collection: Task[];
  private isNotAllowed: boolean = true;
  private taskTitle;
  private modalRef: BsModalRef;
  constructor(private _todoService: TodoService, private modalService: BsModalService) {

  }

  onSearch(search: string): void {
    this.searchTerms.next(search);
  }

  @ViewChild(SortableComponent) sortableComponent: SortableComponent;
  ngOnInit() {
    this.searchTerms
      .debounceTime(300)
      .distinctUntilChanged()
      .map(searchText => {
        this.isNotAllowed = searchText.trim() === '';
        return this._todoService.onSearchTask(searchText);
      })
      .subscribe(subscription => {
        subscription.subscribe(result => {
          this.collection = result;
        })
      });

    this._todoService.getTasks().subscribe(result => {
      this.collection = result;
    });
  }

  addTodo() {
    const task: Task = new Task(uuidv4(), this.taskTitle, false, 0);
    this._todoService.setItem(task);
    this.collection.push(task);
    this.sortableComponent.writeValue(this.collection);
  }

  removeTodo(task) {
    this._todoService.removeItem(task);
  }

  setTaskAsDone(task: Task) {
    task.isMarkedAsDone = true;
    this._todoService.editTask(task);
  }

  onChange(result) {
    if (!result.length) return;
    this._todoService.reorderTasks(result);

  }
  public openModal(template: TemplateRef<any>, task: Task) {
    this.modalRef = this.modalService.show(template);

  }

  public showConfirmationModal(task: Task): void {
    const modal = this.modalService.show(ModalComponent);
    (<ModalComponent>modal.content).showConfirmationModal(task.title);

    (<ModalComponent>modal.content).onClose.subscribe(result => {
      if (!result) return;
      this.removeTodo(task);
      const collection = this.collection;
      collection.splice(collection.findIndex(e => e.id === task.id), 1);
      this.sortableComponent.writeValue(collection);
    });
  }
}
