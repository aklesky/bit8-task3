export interface BaseModel {
  update(item:any);
}

export interface StorageProvider {
  getAll(): Array<any>;
  save(): StorageProvider;
  update(newValue: any): StorageProvider;
};
